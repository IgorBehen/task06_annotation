package com.ihorbehen.unknown_type_object;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class CreateUnknownTypeObject {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        CreateUnknownType myClass = null;
        try {
            Class clazz = Class.forName(CreateUnknownType.class.getName());
            Class[] params = {int.class, String.class};
            myClass = (CreateUnknownType) clazz.getConstructor(params).newInstance(1, "default2");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(myClass);
    }
}






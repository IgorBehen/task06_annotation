package com.ihorbehen.unknown_type_object;

public class CreateUnknownType {
    private int age;
    private String name;

    CreateUnknownType(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public CreateUnknownType() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

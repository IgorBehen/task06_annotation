package com.ihorbehen.my_annotation;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PACKAGE, ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
public @interface MyAnniotation {
    enum Priority {
        LOW, MEDIUM, HIGH
    }

    String value();

    Priority pri() default Priority.MEDIUM;

    String str() default "Java";

    double d();

    int i() default 8;

    String[] arr();
}

package com.ihorbehen.my_annotation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Create your own annotation. Create class with a few fields,
 * some of which annotate with this annotation.
 * Through reflection print those fields in the class that were annotate by this annotation.
 */

public class Test_tasks {

    private static final Logger log = LogManager.getLogger(Test_tasks.class.getName());

    @MyAnniotation(value = "bool_string", d = 21.32, arr = {"a", "b", "c"})
    Boolean bool = false;

    @MyAnniotation(value = "bool_string", d = 21.32, arr = {"a", "b", "c"})
    int num = 125;

    @MyAnniotation(value = "bool_string", d = 21.32, arr = {"a", "b", "c"})
    String name = "Anna";

    @MyAnniotation(value = "bool_string", d = 21.32, arr = {"a", "b", "c"})
    double longNum = 0.84;

    private Test_tasks(int i, String test) {
    }

    @MyAnniotation(pri = MyAnniotation.Priority.LOW, value = "somestring", d = 3.14, arr = {"1", "2", "3"})
    private void doSomeThing() {
        System.out.println("This is a method - 'doSomeThing'");
    }

    @MyAnnotation1(enabled = true)
    @MyAnniotation(pri = MyAnniotation.Priority.LOW, value = "somestring", d = 3.14, arr = {"1", "2", "3"})
    private void doSomeThing1() {
        System.out.println("This is a method - 'doSomeThing1'");
    }

    @MyAnnotation1(enabled = false)
    @MyAnniotation(pri = MyAnniotation.Priority.LOW, value = "somestring", d = 3.14, arr = {"1", "2", "3"})
    private void doSomeThing2() {
        System.out.println("This is a method - 'doSomeThing2'");
    }

    @MyAnniotation(str = "JAVA", i = 11, d = 1.9, value = "New", arr = {"Epam", "Lviv"})
    public static void main(String[] args) throws NoSuchMethodException {
        Test_tasks test_tasks1 = new Test_tasks(12, "test_v");
        test_tasks1.fildsOfMyAnnotMark();
    }

    private void fildsOfMyAnnotMark() {
        Test_tasks test_tasks = new Test_tasks(12, "test_v");
        Class<?> test_tasks_obj = test_tasks.getClass();
        for (Field field : test_tasks_obj.getDeclaredFields()) {
            Class type = field.getType();
            String name = field.getName();
            Annotation[] annotations = field.getDeclaredAnnotations();
            System.out.println("Type:" + type + "  Name:" + name + "  Annotation:" + Arrays.toString(annotations));
        }
    }
}




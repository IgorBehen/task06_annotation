package com.ihorbehen.invoke_of_methods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;

/**
 * Invoke method (three method with different parameters and return types)
 */

public class InvokeOfMethods {
    private static final Logger log = LogManager.getLogger(InvokeOfMethods.class.getName());

    public static void main(String[] args) throws NoSuchMethodException, ClassNotFoundException {

        Class<InvokeOfMethods> c = InvokeOfMethods.class;
        Method m = c.getDeclaredMethod("show");
        log.info(m);

        m = c.getDeclaredMethod("showInteger", int.class);
        log.info(m);

        m = c.getDeclaredMethod("showString", double.class);
        log.info(m);
    }

    private Integer show() {
        return 1;
    }

    public int i = 78655;

    public void showInteger(int i) {
        this.i = i;
    }

    private double d = 11.6;

    private double showString(double d) {
        this.d = d;
        return d;
    }
}

package com.ihorbehen.invoke_varArgs_methods;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Invoke myMethod(String a, int ... args) and myMethod(String … args).
 */

public class InvokeVarArgsMethods {

    public InvokeVarArgsMethods() {
    }

    private void varArgMethod(String string, Integer... var) {
        System.out.println("'varArgMethod(), with two arguments called'");
        System.out.println("'string': " + string + "\n'number of args':  " + var.length + "\n'values':");
        for (int i = 0; i < var.length; i++) {
            System.out.println("      args " + i + ": " + var[i]);
        }
        System.out.println();
    }

    private void varArgMethod1(String... string) {
        System.out.println("'varArgMethod(), with one argument called'");
        System.out.println("'number of args':  " + string.length + "\n'values':");
        for (int i = 0; i < string.length; i++) {
            System.out.println("      args " + i + ": " + string[i]);
        }
        System.out.println();
    }

    public static void main(String[] args) throws Exception {
        InvokeVarArgsMethods ivam = new InvokeVarArgsMethods();
        //varArgMethod() calling
        Class[] parameterTypes = new Class[]{String.class, Integer[].class};
        Method varMeth = ivam.getClass().getDeclaredMethod("varArgMethod", parameterTypes);
        System.out.println("'varMeth' = " + varMeth);
        Object[] arguments = new Object[]{"Text string ", new Integer[]{28, 3, 92}};
        varMeth.invoke(ivam, arguments);
        //varArgMethod1() calling
        Class[] parameterTypes1 = new Class[]{String[].class};
        Method varMeth1 = ivam.getClass().getDeclaredMethod("varArgMethod1" + Arrays.toString(parameterTypes1));
        System.out.println("'varMeth1' = " + varMeth1);
        Object[] arguments1 = new Object[]{"a", "b"};
        varMeth1.invoke(ivam, arguments1);
    }
}

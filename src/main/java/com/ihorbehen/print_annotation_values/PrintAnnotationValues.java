package com.ihorbehen.print_annotation_values;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/**
 * Print annotation value into console (e.g. @Annotation(name = "111"))
 */

@Retention(RetentionPolicy.RUNTIME)
@interface OwnAnnotation {
    String name();

    int age();
}

public class PrintAnnotationValues {
    private static final Logger log = LogManager.getLogger(PrintAnnotationValues.class.getName());

    @OwnAnnotation(name = "John", age = 35)
    public static void getOwnAnnotation() {
        try {
            Class c = PrintAnnotationValues.class;
            Method[] methods = c.getMethods();
            Method method = null;
            for (Method m : methods) {
                if (m.getName().equals("getOwnAnnotation"))
                    method = m;
            }
            OwnAnnotation ownAnno = method.getAnnotation(OwnAnnotation.class);
            log.info("Annotation for Method Object having name: " + method.getName());
            log.info("Annotation attribute - name is: " + ownAnno.name());
            log.info("Annotation attribute - age is : " + ownAnno.age());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        getOwnAnnotation();
    }
}

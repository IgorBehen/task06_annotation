package com.ihorbehen.set_unknowing_type_value;

import java.lang.reflect.Field;

/**
 * Set value into field not knowing its type.
 */

public class SetUnknowingTypeValue {
    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException {
        Person p = new Person();
        SetUnknowingTypeValue.setInstanceValue(p);
        SetUnknowingTypeValue.setInstanceValue1(p);
        SetUnknowingTypeValue.setInstanceValue2(p);
    }

    public static void setInstanceValue(Person p) throws SecurityException,
            NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
        final Field field = p.getClass().getDeclaredField("name");
        field.setAccessible(true);
        System.out.println("get public String: " + field.get(p));
        field.set(p, "Eva");
        System.out.println("set public String: " + field.get(p));
    }

    public static void setInstanceValue1(Person p) throws SecurityException,
            NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
        final Field field = p.getClass().getDeclaredField("age");
        field.setAccessible(true);
        System.out.println("get private int: " + field.get(p));
        field.set(p, 18);
        System.out.println("set private int: " + field.get(p));
    }

    public static void setInstanceValue2(Person p) throws SecurityException,
            NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
        final Field field = p.getClass().getDeclaredField("height");
        field.setAccessible(true);
        System.out.println("get private static double: " + field.get(p));
        field.set(p, 1.8);
        System.out.println("set private static double: " + field.get(p));
    }
}
